package com.example.advjavaspring.repository;

import com.example.advjavaspring.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer > {
    @Query(value = "select * from student where id = 101", nativeQuery = true)
    Student getStudent();
    List<Student> findAllByStudId(int studId);

}
