package com.example.advjavaspring.repository;

import com.example.advjavaspring.model.Group;
import com.example.advjavaspring.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Integer > {
    @Query(value = "select * from group_table where id = 1901", nativeQuery = true)
    Student getGroup();
    List<Student> findAllById(int Id);
}
