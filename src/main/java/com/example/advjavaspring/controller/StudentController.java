package com.example.advjavaspring.controller;

import com.example.advjavaspring.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/api/students")
    public ResponseEntity<?> getStudents() {
        return ResponseEntity.ok(studentRepository.findAll());
    }
    @GetMapping("/api/students/{studentId}")
    public ResponseEntity<?> getStudent(@PathVariable int studentId) {
        return ResponseEntity.ok(studentRepository.findById(studentId));
    }



}
