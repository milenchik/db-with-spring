package com.example.advjavaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvjavaspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvjavaspringApplication.class, args);
	}

}
